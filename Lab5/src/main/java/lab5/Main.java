package lab5;

import com.microcrowd.loader.java3d.max3ds.Loader3DS; 

import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.*;

import java.awt.Color;
import javax.media.j3d.*;
import javax.media.j3d.Material;
import javax.vecmath.*;
import javax.media.j3d.Background;

import com.sun.j3d.loaders.*;
import com.sun.j3d.loaders.objectfile.ObjectFile;
import com.sun.j3d.loaders.lw3d.Lw3dLoader;
import com.sun.j3d.utils.image.TextureLoader;
import java.awt.*;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import javax.swing.JFrame;


public class Main extends JFrame {
    static SimpleUniverse universe;
    static Scene scene;
    static Map<String, Shape3D> nameMap;
    static Canvas3D canvas;

    BranchGroup root;
    Animation animation;
    
    public Main () throws IOException{
        configureWindow();
        configureCanvas();
        configureUniverse();
        setupScene();

        addImageBackground();
        addLightToUniverse();
        addOtherLight();
        ChangeViewAngle();
        root.compile();
        universe.addBranchGraph(root);

        new Animator(animation);
    }

    private void setupScene() throws IOException {
        Transform3D tf;

        root = new BranchGroup();
        Scene building = getSceneFromFile("building.obj");
        addTextureAppearance((Shape3D)building.getNamedObjects().get("default"), getTexture("bricks.jpg"), getMaterial());

        TransformGroup buildingTfg = new TransformGroup();
        tf = new Transform3D();
        tf.setTranslation(new Vector3d(-0.2, -1.0, 0.0));
        tf.setScale(1.5);
        buildingTfg.setTransform(tf);
        buildingTfg.addChild(building.getSceneGroup());

        Scene[] doors = new Scene[4];
        TransformGroup[] doorTfgs = new TransformGroup[4];
        for (int i = 0; i < 4; i++) {
            doors[i] = getSceneFromFile("door.obj");
            Scene door = doors[i];
            Material mtl = new Material();
            mtl.setDiffuseColor(new Color3f(1.0f, 1.0f, 1.0f));
            addTextureAppearance((Shape3D)door.getNamedObjects().get("default"), getTexture("steel.jpg"), mtl);
            TransformGroup tfg = new TransformGroup();

            tf = new Transform3D();
            tf.setScale(0.5);
            tf.setTranslation(new Vector3d(-0.22, -0.75 + 0.5 * i, -0.05));
            tfg.setTransform(tf);

            tfg.addChild(door.getSceneGroup());
            doorTfgs[i] = new TransformGroup();
            doorTfgs[i].addChild(tfg);
            doorTfgs[i].setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
            buildingTfg.addChild(doorTfgs[i]);
        }

        Scene[] people = new Scene[4];
        TransformGroup[] peopleRootTfgs = new TransformGroup[people.length];
        TransformGroup[] peopleTfgs = new TransformGroup[people.length];
        for (int i = 0; i < people.length; i++) {
            people[i] = getSceneFromFile("person.obj");
            Scene person = people[i];
            peopleRootTfgs[i] = new TransformGroup();
            TransformGroup rtfg = peopleRootTfgs[i];
            peopleTfgs[i] = new TransformGroup();
            TransformGroup tfg = peopleTfgs[i];

            tf = new Transform3D();
            tf.rotY(3.14 / 2);
            tf.setScale(0.2);
            tf.setTranslation(new Vector3d(0.5, personGetFloorY(i), 0));
            rtfg.setTransform(tf);

            rtfg.addChild(person.getSceneGroup());
            rtfg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
            tfg.addChild(rtfg);
            tfg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
            buildingTfg.addChild(tfg);
        }

        Scene elevator = getSceneFromFile("door.obj");
        addTextureAppearance((Shape3D)elevator.getNamedObjects().get("default"), getTexture("steel.jpg"), new Material());
        TransformGroup elevatorTfg;
        {
            TransformGroup tfg = new TransformGroup();
            tf = new Transform3D();
            tf.rotZ(-3.14 / 2);
            tf.setScale(0.4);
            tf.setTranslation(new Vector3d(-0.45, -0.94, 0));
            tfg.setTransform(tf);

            tfg.addChild(elevator.getSceneGroup());
            elevatorTfg = new TransformGroup();
            elevatorTfg.addChild(tfg);
            elevatorTfg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
            buildingTfg.addChild(elevatorTfg);
        }

        animation = new SequentialAnimation(new Animation[]{
            new ParallelAnimation(new Animation[] {
                openDoor(true, doorTfgs[0]),
                new InterpolateXTo(2, -1, peopleTfgs[0])
            }),
            openDoor(false, doorTfgs[0]),
            new ParallelAnimation(new Animation[] {
                new InterpolateYTo(1, getFloorY(1), elevatorTfg),
                new InterpolateYTo(1, personGetFloorY(1), peopleRootTfgs[0])
            }),
            openDoor(true, doorTfgs[1]),
            new ParallelAnimation(new Animation[] {
                new InterpolateXTo(2, -0.5, peopleTfgs[0]),
                new InterpolateXTo(2, -1, peopleTfgs[1])
            }),
            openDoor(false, doorTfgs[1]),
            new ParallelAnimation(new Animation[] {
                new InterpolateYTo(2, getFloorY(3), elevatorTfg),
                new InterpolateYTo(2, personGetFloorY(3), peopleRootTfgs[1])
            }),
            openDoor(true, doorTfgs[3]),
            new ParallelAnimation(new Animation[] {
                new InterpolateXTo(2, -0.5, peopleTfgs[1]),
                new InterpolateXTo(2, -1.07, peopleTfgs[3])
            }),
            openDoor(false, doorTfgs[3]),
            new ParallelAnimation(new Animation[] {
                new InterpolateYTo(1, getFloorY(2), elevatorTfg),
                new InterpolateYTo(1, personGetFloorY(2), peopleRootTfgs[3])
            }),
            openDoor(true, doorTfgs[2]),
            new InterpolateXTo(2, -0.95, peopleTfgs[2]),
            openDoor(false, doorTfgs[2]),
            new ParallelAnimation(new Animation[] {
                new InterpolateYTo(2, getFloorY(0), elevatorTfg),
                new InterpolateYTo(2, personGetFloorY(0), peopleRootTfgs[3]),
                new InterpolateYTo(2, personGetFloorY(0), peopleRootTfgs[2])
            }),
            openDoor(true, doorTfgs[0]),
            new ParallelAnimation(new Animation[] {
                new InterpolateXTo(2, 0, peopleTfgs[3]),
                new InterpolateXTo(2, -0.3, peopleTfgs[2]),
                new SequentialAnimation(new Animation[] {
                    new NoopAnimation(1),
                    openDoor(false, doorTfgs[0])
                }, false)
            })
        }, false);

        root.addChild(buildingTfg);
    }

    private static Animation openDoor(boolean open, TransformGroup tfg) {
        return new InterpolateZTo(1, open ? -1.0 : 0, tfg);
    }

    private static double getFloorY(int floor) {
        return 0.5 * (floor) - 0.03 * floor;
    }

    private static double personGetFloorY(int floor) {
        return getFloorY(floor+1) - 1.25;
    }

    private static String getResourcePath(String resource) {
        return resource;
    }
    
    private void configureWindow()  {
        setTitle("Lab5");
        setSize(760,640);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    private void configureCanvas(){
        canvas=new Canvas3D(SimpleUniverse.getPreferredConfiguration());
        getContentPane().add(canvas,BorderLayout.CENTER);
    }
    
    private void configureUniverse(){
        root= new BranchGroup();
        universe= new SimpleUniverse(canvas);
        universe.getViewingPlatform().setNominalViewingTransform();
    }
    
    private void addLightToUniverse(){
        Bounds bounds = new BoundingSphere(new Point3d(0, 0, 0), 10);
        Color3f color = new Color3f(1.0f, 1.0f, 1.0f);
        Vector3f lightdirection = new Vector3f(-0.5f,-1f,-1f);
        DirectionalLight dirlight = new DirectionalLight(color,lightdirection);
        dirlight.setInfluencingBounds(bounds);
        root.addChild(dirlight);
    }
    
    Texture getTexture(String name) {
        TextureLoader textureLoader = new TextureLoader(getResourcePath(name), "LUMINANCE", canvas);
        Texture texture = textureLoader.getTexture();
        texture.setBoundaryModeS(Texture.WRAP);
        texture.setBoundaryModeT(Texture.WRAP);
        texture.setBoundaryColor( new Color4f( 0.0f, 1.0f, 0.0f, 0.0f ) );
        texture.setAnisotropicFilterMode(Texture.ANISOTROPIC_SINGLE_VALUE);
        texture.setAnisotropicFilterDegree(15.0f);
        return texture;
    }             
    
    Material getMaterial() {
        Material material = new Material();
        material.setAmbientColor ( new Color3f( 0.33f, 0.26f, 0.23f ) );
        material.setDiffuseColor ( new Color3f( 0.50f, 0.11f, 0.00f ) );
        return material;
    }
    
    private void addTextureAppearance(Shape3D shape, Texture texture, Material material){
        Appearance app = new Appearance();
        app.setTexture(texture);
        app.setMaterial(material);

        TextureAttributes attr = new TextureAttributes();
        attr.setTextureMode(TextureAttributes.REPLACE);
        app.setTextureAttributes(attr);

        shape.setAppearance(app);
    }
    
    private void addImageBackground(){
        TextureLoader t = new TextureLoader("background.jpg", canvas);
        Background background = new Background(t.getImage());
        background.setImageScaleMode(Background.SCALE_FIT_ALL);
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),100.0);
        background.setApplicationBounds(bounds);
        root.addChild(background);
    }
    
    private void ChangeViewAngle(){
        ViewingPlatform vp = universe.getViewingPlatform();
        TransformGroup vpGroup = vp.getMultiTransformGroup().getTransformGroup(0);
        Transform3D vpTranslation = new Transform3D();
        Vector3f translationVector = new Vector3f(0.0F, -1.2F, 6F);
        vpTranslation.setTranslation(translationVector);
        vpGroup.setTransform(vpTranslation);
    }
    
    private void addOtherLight() {
        Color3f directionalLightColor = new Color3f(Color.BLACK);
        Color3f ambientLightColor = new Color3f(Color.WHITE);
        Vector3f lightDirection = new Vector3f(-1F, -1F, -1F);

        AmbientLight ambientLight = new AmbientLight(ambientLightColor);
        DirectionalLight directionalLight = new DirectionalLight(directionalLightColor, lightDirection);

        Bounds influenceRegion = new BoundingSphere(new Point3d(0, 0, 0), 10);

        ambientLight.setInfluencingBounds(influenceRegion);
        directionalLight.setInfluencingBounds(influenceRegion); 
        root.addChild(ambientLight);
        root.addChild(directionalLight);
    }
   
    public static Scene getSceneFromFile(String location) throws IOException {
        ObjectFile file = new ObjectFile(ObjectFile.RESIZE);
        file.setFlags (ObjectFile.RESIZE | ObjectFile.TRIANGULATE | ObjectFile.STRIPIFY);
        return file.load(new FileReader(getResourcePath(location)));
    }
    
    //Not always works
    public static Scene getSceneFromLwoFile(String location) throws IOException {
        Lw3dLoader loader = new Lw3dLoader();
        return loader.load(new FileReader(location));
     }
    
    public static void main(String[]args){
        try {
            Main window = new Main();
            window.setVisible(true);
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
