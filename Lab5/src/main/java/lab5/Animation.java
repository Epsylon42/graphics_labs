package lab5;


public abstract class Animation {
    private double duration;

    public Animation(double duration) {
        this.duration = duration;
    }

    public double getDuration() {
        return duration;
    }

    public void start() {}

    public abstract void apply(double time);
}
