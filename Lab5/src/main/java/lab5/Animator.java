package lab5;

import javax.swing.Timer;
import java.awt.event.*;

public class Animator implements ActionListener {
    private Timer timer;
    private double t;
    private Animation anim;

    public Animator(Animation anim) {
        this.anim = anim;
        timer = new Timer(10, this);
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() != timer) {
            return;
        }
        t += 0.01;

        anim.apply(t);
    }
}
