package lab5;

import java.util.Arrays;


public class SequentialAnimation extends Animation {
    private Animation[] animations;
    private boolean[] started;
    private boolean loop;

    public SequentialAnimation(Animation[] animations, boolean loop) {
        super(Arrays
                .stream(animations)
                .map(a -> a.getDuration())
                .reduce(0.0, (acc, x) -> acc + x)
                );
        this.animations = animations;
        this.loop = loop;
        this.started = new boolean[animations.length];
    }

    @Override
    public void start() {
        for (int i = 0; i < started.length; i++) {
            started[i] = false;
        }
    }

    @Override
    public void apply(double time) {
        if (time > getDuration()) {
            //TODO: reset array after looping
            if (loop) {
                time -= Math.floor(time / getDuration());
            } else {
                time = getDuration();
            }
        }

        for (int i = 0; i < animations.length; i++) {
            Animation a = animations[i];
            if (time <= a.getDuration()) {
                if (!started[i]) {
                    started[i] = true;
                    a.start();
                }
                a.apply(time);
                return;
            } else {
                time -= a.getDuration();
            }
        }
    }
}
