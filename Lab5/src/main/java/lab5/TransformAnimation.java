package lab5;

import javax.media.j3d.*;
import java.util.function.BiConsumer;


public abstract class TransformAnimation extends Animation {
    private TransformGroup tfg;

    public TransformAnimation(double duration, TransformGroup tfg) {
        super(duration);
        this.tfg = tfg;
    }

    public static TransformAnimation fromFn(double duration, TransformGroup tfg, BiConsumer<Double, Transform3D> func) {
        return new TransformAnimation(duration, tfg) {
            public Transform3D getTransform(double frac, Transform3D tf) {
                func.accept(frac, tf);
                return tf;
            }
        };
    }

    @Override
    public void apply(double time) {
        Transform3D tf = new Transform3D();
        tfg.getTransform(tf);
        tfg.setTransform(getTransform(time / getDuration(), tf));
    }

    public abstract Transform3D getTransform(double frac, Transform3D tf);
}
