package lab5;

import javax.media.j3d.*;
import javax.vecmath.*;


public class InterpolateY extends TransformAnimation {
    private double a;
    private double b;

    public InterpolateY(double duration, double a, double b, TransformGroup tfg) {
        super(duration, tfg);
        this.a = a;
        this.b = b;
    }

    @Override
    public Transform3D getTransform(double frac, Transform3D tf) {
        if (frac > 1) {
            frac = 1;
        }

        tf.setTranslation(new Vector3d(0.0, a + (b - a) * frac, 0.0));
        return tf;
    }
}
