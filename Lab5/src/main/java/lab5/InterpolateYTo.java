package lab5;

import javax.media.j3d.*;
import javax.vecmath.*;


public class InterpolateYTo extends TransformAnimation {
    private double a;
    private double b;
    private boolean first = true;
    Matrix3f m = new Matrix3f();

    public InterpolateYTo(double duration, double b, TransformGroup tfg) {
        super(duration, tfg);
        this.b = b;
    }

    @Override
    public void start() {
        first = true;
    }

    @Override
    public Transform3D getTransform(double frac, Transform3D tf) {
        if (first) {
            first = false;
            Vector3d v = new Vector3d();
            tf.get(m, v);
            a = v.y;
        }

        if (frac > 1) {
            frac = 1;
        }

        Vector3d v = new Vector3d();
        tf.get(m, v);
        v.y = a + (b - a) * frac;

        tf.setTranslation(v);
        return tf;
    }
}
