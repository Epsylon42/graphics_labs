package HelloWorld;
 
import javafx.scene.paint.Color;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.shape.*;

public class MyJavaFXApplication extends Application {
       
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
//        primaryStage.setTitle("Hello World!");

        final int width = 300;
        final int height = 250;
        
        Group root = new Group();
        Scene scene = new Scene(root, width, height);

        Rectangle bg = new Rectangle();
        bg.setX(0);
        bg.setY(0);
        bg.setWidth(width);
        bg.setHeight(height);
        bg.setFill(Color.rgb(128, 128, 0));
        root.getChildren().add(bg);

        double wallWidth = (double)width * 8 / 10;
        double wallHeight = (double)height * 8 / 10;
        Rectangle wallBg = new Rectangle();
        wallBg.setX(width / 10);
        wallBg.setY(height / 10);
        wallBg.setWidth(wallWidth);
        wallBg.setHeight(wallHeight);
        wallBg.setFill(Color.rgb(128, 0, 0));
        root.getChildren().add(wallBg);

        int numHorizontal = 3;
        for (int i = 0; i < numHorizontal; i++) {
            Line line;

            double startX = wallBg.getX() + 2.5;
            double endX = wallBg.getX() + wallWidth - 2.5;
            double y = wallBg.getY() + wallHeight * (i+1) / numHorizontal;

            if (i != numHorizontal-1) {
                line = new Line();

                line.setStartX(startX);
                line.setEndX(endX);

                line.setStartY(y);
                line.setEndY(y);

                line.setStrokeWidth(5);
                line.setStroke(Color.rgb(255, 255, 0));

                root.getChildren().add(line);
            }

            int numVertical = i % 2 == 0 ? 1 : 3;
            for (int j = 0; j < numVertical; j++) {
                if (j % 2 != 0) {
                    continue;
                }
                line = new Line();

                double x = startX + wallWidth * (j+1) / (numVertical+1);
                line.setStartX(x);
                line.setEndX(x);

                line.setStartY(y - wallHeight / 3 + 2.5);
                line.setEndY(y - 2.5);

                line.setStrokeWidth(5);
                line.setStroke(Color.rgb(255, 255, 0));

                root.getChildren().add(line);
            }
        }

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
