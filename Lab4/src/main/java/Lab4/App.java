package Lab4;

import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.SimpleUniverse;

import java.awt.*;
import java.awt.event.*;
import javax.swing.Timer;

public class App implements ActionListener {
    Transform3D pencilTransform = new Transform3D();
    Vector3d euler = new Vector3d(0.0, 0.0, 0.0);
    TransformGroup pencilGroup;

    public static void main(String[] args) {
        App app = new App();
        app.init();
    }

    public void init() {
        new Timer(25, this).start();

        SimpleUniverse universe = new SimpleUniverse();
        universe.getViewingPlatform().setNominalViewingTransform();

        BranchGroup pencil = makePencil();
        TransformGroup tfg = new TransformGroup();
        tfg.addChild(pencil);
        tfg.setTransform(pencilTransform);
        tfg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

        pencilGroup = tfg;

        BranchGroup group = new BranchGroup();
        group.addChild(tfg);
        group.addChild(makeLight());
        universe.addBranchGraph(group);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (pencilGroup != null) {
            euler.setZ(euler.getZ() + 0.01);
            euler.setX(euler.getX() + 0.01);
            pencilTransform.setEuler(euler);
            pencilGroup.setTransform(pencilTransform);
        }
    }

    private static BranchGroup makePencil() {
        BranchGroup pencil = new BranchGroup();
        
        Cylinder c = new Cylinder(0.07f, 1.0f);
        c.setAppearance(makeColor(new Color3f(0.1f, 0.8f, 0.8f), false));

        Transform3D tf = new Transform3D();
        tf.setTranslation(new Vector3d(0.0, -0.5, 0.0));
        Cylinder eraserMetalThing = new Cylinder(0.073f, 0.1f);
        eraserMetalThing.setAppearance(makeColor(new Color3f(0.7f, 0.4f, 0.1f), true));
        eraserMetalThing.getAppearance().getMaterial().setSpecularColor(new Color3f(1.0f, 0.4f, 0.2f));
        pencil.addChild(applyTransform(eraserMetalThing, tf));

        tf = new Transform3D();
        tf.setTranslation(new Vector3d(0.0, -0.55, 0.0));
        Sphere eraser = new Sphere(0.07f);
        eraser.setAppearance(makeColor(new Color3f(1.0f, 0.5f, 0.5f), false));
        pencil.addChild(applyTransform(eraser, tf));

        tf = new Transform3D();
        tf.setTranslation(new Vector3d(0.0, 0.6, 0.0));
        Cone cone = new Cone(0.07f, 0.2f);
        cone.setAppearance(makeColor(new Color3f(0.6f, 0.4f, 0.0f), false));
        pencil.addChild(applyTransform(cone, tf));

        tf = new Transform3D();
        tf.setTranslation(new Vector3d(0.0, 0.66, 0.0));
        Cone end = new Cone(0.03f, 0.1f);
        end.setAppearance(makeColor(new Color3f(0.4f, 0.4f, 0.4f), true));
        pencil.addChild(applyTransform(end, tf));

        pencil.addChild(c);
        return pencil;
    }

    private static TransformGroup applyTransform(Node object, Transform3D tf) {
        TransformGroup group = new TransformGroup();
        group.setTransform(tf);
        group.addChild(object);
        return group;
    }

    private static Appearance makeColor(Color3f color, boolean specular) {
        Appearance a = new Appearance();
        a.setMaterial(new Material(color, new Color3f(), color, specular ? color : new Color3f(), 1.0f));
        return a;
    }

    private static DirectionalLight makeLight() {
        Color3f lightColor = new Color3f(1.0f, 1.0f, 1.0f);
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
        Vector3f lightDirection = new Vector3f(4.0f, -7.0f, -12.0f);
        DirectionalLight light = new DirectionalLight(lightColor, lightDirection);
        light.setInfluencingBounds(bounds);
        return light;
    }
}
