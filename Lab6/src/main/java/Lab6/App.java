package Lab6;

import com.sun.j3d.utils.universe.*;

import javax.media.j3d.*;
import javax.media.j3d.Material;
import javax.vecmath.*;

import com.sun.j3d.loaders.*;
import com.sun.j3d.loaders.objectfile.ObjectFile;
import java.io.FileReader;
import javax.swing.JFrame;

import Lab6.animation.*;


public class App extends JFrame {
    SimpleUniverse universe;
    Scene scene;
    Canvas3D canvas;

    BranchGroup root;
    TransformGroup goose;
    TransformGroup beak;
    TransformGroup left_leg;
    TransformGroup right_leg;

    public static void main(String[] args) throws Exception {
        App app = new App();
        app.setVisible(true);
    }

    public App() throws Exception {
        setupUniverse();
        setupLight();
        loadModel();
        animate();

        root.compile();
        universe.addBranchGraph(root);
    }

    private void setupUniverse() {
        setTitle("Lab6");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        canvas = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
        getContentPane().add(canvas);

        universe = new SimpleUniverse(canvas);
        universe.getViewingPlatform().setNominalViewingTransform();

        root = new BranchGroup();
    }

    private void setupLight() {
        Bounds bounds = new BoundingSphere(new Point3d(0, 0, 0), 10);
        Color3f color = new Color3f(1f, 1f, 1f);
        Vector3f dir = new Vector3f(-0.5f, -1f, -1f);
        DirectionalLight light = new DirectionalLight(color, dir);
        light.setInfluencingBounds(bounds);
        root.addChild(light);
    }

    private void loadModel() throws Exception {
        ObjectFile file = new ObjectFile(ObjectFile.RESIZE);
        file.setFlags(ObjectFile.RESIZE | ObjectFile.TRIANGULATE | ObjectFile.STRIPIFY);
        Scene gooseScene = file.load(new FileReader("goose.obj"));
        for (Object key : gooseScene.getNamedObjects().keySet()) {
            if (!key.equals("body")) {
                Shape3D el = (Shape3D)gooseScene.getNamedObjects().get(key);
                Appearance app = el.getAppearance();
                Material mtl = app.getMaterial();
                mtl.setDiffuseColor(1.0f, 0.35f, 0.0f);
                app.setMaterial(mtl);
                el.setAppearance(app);
            }
        }
        
        goose = new TransformGroup();
        goose.addChild(((Shape3D)gooseScene.getNamedObjects().get("body")).cloneTree());

        beak = new TransformGroup();
        beak.addChild(((Shape3D)gooseScene.getNamedObjects().get("beak")).cloneTree());

        left_leg = new TransformGroup();
        left_leg.addChild(((Shape3D)gooseScene.getNamedObjects().get("left_leg")).cloneTree());

        right_leg = new TransformGroup();
        right_leg.addChild(((Shape3D)gooseScene.getNamedObjects().get("right_leg")).cloneTree());

        goose.addChild(beak);
        goose.addChild(left_leg);
        goose.addChild(right_leg);


        TransformGroup actualGoose = new TransformGroup();
        Transform3D tf = new Transform3D();
        tf.setScale(0.2);
        actualGoose.setTransform(tf);
        actualGoose.addChild(goose);


        root.addChild(actualGoose);
    }

    private void animate() {
        float angle = (float)Math.PI/8;
        int duration = 750;

        Transform3D axis = new Transform3D();
        axis.rotZ(Math.PI/2);
        Alpha a = new Alpha(-1, duration);
        a.setMode(Alpha.INCREASING_ENABLE | Alpha.DECREASING_ENABLE);
        a.setDecreasingAlphaDuration(duration);
        RotationInterpolator rotation = new RotationInterpolator(a, left_leg, axis, -angle, angle);

        Bounds bounds = new BoundingSphere(new Point3d(), 100);
        rotation.setSchedulingBounds(bounds);

        left_leg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        left_leg.addChild(rotation);


        a = new Alpha(-1, duration);
        a.setMode(Alpha.INCREASING_ENABLE | Alpha.DECREASING_ENABLE);
        a.setDecreasingAlphaDuration(duration);
        a.setPhaseDelayDuration(duration);
        rotation = new RotationInterpolator(a, right_leg, axis, -angle, angle);
        rotation.setSchedulingBounds(bounds);

        right_leg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        right_leg.addChild(rotation);

        Animation anim = new SequentialAnimation(new Animation[] {
            new AngleFromTo(2, new Vector3d(0, 1, 0), 0, -Math.PI/2, goose).quad(2),
            new InterpolateXTo(4, -3, goose).quad(4),
            new SequentialAnimation(new Animation[] {
                new ParallelAnimation(new Animation[] {
                    new AngleFromTo(2, new Vector3d(0,1,0), -Math.PI/2, Math.PI/2, goose).quad(2),
                    new SequentialAnimation(new Animation[] {
                        new NoopAnimation(1.5),
                        new InterpolateXTo(6.5, 3, goose).quad(6.5)
                    }, false)
                }),
                new ParallelAnimation(new Animation[] {
                    new AngleFromTo(2, new Vector3d(0,1,0), Math.PI/2, -Math.PI/2, goose).quad(2),
                    new SequentialAnimation(new Animation[] {
                        new NoopAnimation(1.5),
                        new InterpolateXTo(6.5, -3, goose).quad(6.5)
                    }, false)
                })
            }, true)
        }, false);
        goose.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        new Animator(anim);
    }
}
