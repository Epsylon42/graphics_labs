package Lab6.animation;

import java.util.Arrays;


public class ParallelAnimation extends Animation {
    private Animation[] animations;

    public ParallelAnimation(Animation[] animations) {
        super(Arrays.stream(animations).map(a -> a.getDuration()).max(Double::compare).get());
        this.animations = animations;
    }

    @Override
    public void start() {
        for (Animation a : animations) {
            a.start();
        }
    }

    @Override
    public void apply(double time) {
        for (Animation a : animations) {
            if (time <= a.getDuration()) {
                a.apply(time);
            }
        }
    }
}
