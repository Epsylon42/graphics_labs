package Lab6.animation;


class Quadratic extends Animation {
    private Animation inner;
    private double quadraticTime;

    public Quadratic(Animation inner, double quadraticTime) {
        super(inner.getDuration());
        this.inner = inner;
        this.quadraticTime = quadraticTime;
    }

    @Override
    public void apply(double time) {
        if (time > quadraticTime) {
            inner.apply(time);
        } else {
            inner.apply(Math.pow(time, 2) / quadraticTime);
        }
    }
}
