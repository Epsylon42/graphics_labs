package Lab6.animation;

import java.util.Arrays;


public class SequentialAnimation extends Animation {
    private Animation[] animations;
    private boolean[] started;
    private boolean loop;
    private double loopDuration;

    public SequentialAnimation(Animation[] animations, boolean loop) {
        super(loop ? Double.POSITIVE_INFINITY : calculateLoopDuration(animations));
        loopDuration = getDuration() == Double.POSITIVE_INFINITY ? calculateLoopDuration(animations) : getDuration();

        this.animations = animations;
        this.loop = loop;
        this.started = new boolean[animations.length];
    }

    private static double calculateLoopDuration(Animation[] animations) {
        return Arrays
                .stream(animations)
                .map(a -> a.getDuration())
                .reduce(0.0, (acc, x) -> acc + x);
    }

    @Override
    public void start() {
        for (int i = 0; i < started.length; i++) {
            started[i] = false;
        }
    }

    @Override
    public void apply(double time) {
        if (time > loopDuration) {
            if (loop) {
                time -= Math.floor(time / loopDuration) * loopDuration;
            } else {
                time = getDuration();
            }
        }

        for (int i = 0; i < animations.length; i++) {
            Animation a = animations[i];
            if (time <= a.getDuration()) {
                if (!started[i]) {
                    started[i] = true;
                    a.start();
                }
                a.apply(time);
                return;
            } else {
                time -= a.getDuration();
            }
        }
    }
}
