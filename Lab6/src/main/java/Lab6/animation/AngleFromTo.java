package Lab6.animation;

import javax.media.j3d.*;
import javax.vecmath.*;


public class AngleFromTo extends TransformAnimation {
    double a;
    double b;
    Vector3d axis;

    public AngleFromTo(double duration, Vector3d axis, double a, double b, TransformGroup tfg) {
        super(duration, tfg);

        this.a = a;
        this.b = b;
        this.axis = axis;
    }

    @Override
    public Transform3D getTransform(double frac, Transform3D tf) {
        if (frac > 1) {
            frac = 1;
        }

        tf.setRotation(new AxisAngle4d(axis, a + (b-a)*frac));
        return tf;
    }
}
