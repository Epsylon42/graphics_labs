package Lab6.animation;

public class NoopAnimation extends Animation {
    public NoopAnimation(double duration) {
        super(duration);
    }

    @Override public void apply(double time) {}
}
