package Lab6.animation;


public abstract class Animation {
    private double duration;

    public Animation(double duration) {
        this.duration = duration;
    }

    public double getDuration() {
        return duration;
    }

    public void start() {}

    public abstract void apply(double time);

    public Quadratic quad(double quadraticTime) {
        return new Quadratic(this, quadraticTime);
    }
}
