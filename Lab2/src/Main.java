import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

class Pair {
    public double x;
    public double y;

    public Pair(double x, double y) {
        this.x = x;
        this.y = y;
    }
}

@SuppressWarnings("serial")
public class Main extends JPanel implements ActionListener {
    private static int maxWidth = 500;
    private static int maxHeight = 500;

    private static double t = 0.0;
    private Timer timer;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Hello, Java2D!");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(maxWidth, maxHeight);
        frame.setResizable(false);

        frame.add(new Main());
        frame.setVisible(true);
    }

    Main() {
        timer = new Timer(10, this);
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        t += 0.01;
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setBackground(new Color(0.5f, 0.5f, 0.0f));
        g2d.fillRect(0, 0, maxWidth, maxHeight);

        drawWall(g2d);
        ellipse(g2d);
        animation(g2d);
    }

    private void drawWall(Graphics2D g2d) {
        int rectStartX = 20;
        int rectStartY = 20;
        int rectWidth = 200;
        int rectHeight = 150;

        g2d.setColor(new Color(0.5f, 0.0f, 0.0f));
        g2d.fillRect(rectStartX, rectStartY, rectWidth, rectHeight);

        g2d.setColor(new Color(1.0f, 1.0f, 0.0f));
        g2d.setStroke(new BasicStroke(5, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
        int numHorizontal = 3;
        for (int i = 0; i < numHorizontal; i++) {
            int startX = rectStartX;
            int endX = startX + rectWidth;
            int y = rectStartY + rectHeight * (i+1) / numHorizontal;

            if (i != numHorizontal-1) {
                g2d.drawLine(startX, y, endX, y);
            }

            int numVertical = i % 2 == 0 ? 1 : 3;
            for (int j = 0; j < numVertical; j++) {
                if (j % 2 != 0) {
                    continue;
                }

                int x = startX + rectWidth * (j+1) / (numVertical+1);
                g2d.drawLine(x, y - rectHeight / 3, x, y);
            }
        }
    }

    private void ellipse(Graphics2D g2d) {
        AffineTransform oldTf = g2d.getTransform();
        g2d.translate(0, 300);
        g2d.setStroke(new BasicStroke(10, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));

        g2d.setColor(new Color(0.0f, 0.5f, 1.0f));
        double points[][] = {
            {100, 0},
            {200, 50},
            {0, 50}
        };
        GeneralPath path = new GeneralPath();

        path.moveTo(points[0][0], points[0][1]);
        for (int i = 1; i < points.length; i++) {
            path.lineTo(points[i][0], points[i][1]);
        }
        path.closePath();
        g2d.draw(path);

        g2d.setPaint(new GradientPaint(5, 25, new Color(1.0f, 1.0f, 0.0f), 20, 2, new Color(0.0f, 0.0f, 1.0f), true));
        g2d.draw(new Ellipse2D.Double(0, 0, 200, 100));
        g2d.setTransform(oldTf);
    }

    private void animation(Graphics2D g2d) {
        AffineTransform oldTf = g2d.getTransform();
        g2d.translate(250, 100);
        g2d.translate(100, 100);

        g2d.setStroke(new BasicStroke(10, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
        g2d.setColor(Color.YELLOW);
        g2d.drawRect(-100, -100, 200, 200);

        g2d.setStroke(new BasicStroke());
        g2d.setColor(Color.RED);
        g2d.drawRect(-50, -50, 100, 100);

        double radius = Math.sin(t);
        radius = (radius*radius + 0.5) * 20;

        double frac = t / 10.0;
        Pair sqCoord = squareCoord(1.0 - (frac - Math.floor(frac)));
        g2d.fill(new Ellipse2D.Double(sqCoord.x*50.0 - radius, sqCoord.y*50.0 - radius, radius*2, radius*2));

        g2d.setTransform(oldTf);
    }


    private static Pair squareCoord(double frac) {
        if (frac > 7.0/8.0 || frac <= 1.0/8.0) {
            if (frac > 7.0/8.0) {
                frac -= 1.0;
            }

            return new Pair(
                1.0,
                frac * 8.0
            );
        } else if (frac > 1.0/8.0 && frac <= 3.0/8.0) {
            return new Pair(
                -(frac - 2.0/8.0) * 8.0,
                1.0
            );
        } else if (frac > 3.0/8.0 && frac <= 5.0/8.0) {
            return new Pair(
                -1.0,
                -(frac - 4.0/8.0) * 8.0
            );
        } else {
            return new Pair(
                (frac - 6.0/8.0) * 8.0,
                -1.0
            );
        }
    }
}
