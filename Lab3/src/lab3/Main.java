package lab3;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.QuadCurveTo;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.Circle;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.SVGPath;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class Main extends Application {
    private String dolphinPath = "dolphin.svg";
    private Document dolphinSVG;

    public static void main(String args[]) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        Scene scene = new Scene (root, 500, 500);		

        Color bodyColor = Color.rgb(57, 161, 216);

        Group dolphin = new Group();
        dolphin.setScaleX(2);
        dolphin.setScaleY(2);
        root.getChildren().add(dolphin);

        TranslateTransition dolphinMovement = new TranslateTransition(Duration.seconds(3), dolphin);
        dolphinMovement.setFromX(200);
        dolphinMovement.setToX(300);
        dolphinMovement.setFromY(200);
        dolphinMovement.setToY(200);
        dolphinMovement.setCycleCount(Animation.INDEFINITE);
        dolphinMovement.setAutoReverse(true);
        dolphinMovement.play();

        Shape rightFin = pathFromSVG("rightFin");
        rightFin.setFill(bodyColor);
        dolphin.getChildren().add(rightFin);
        rotationAboutOrigin(rightFin, 30, 20, 45, 1000).play();

        Shape body = pathFromSVG("body");
        body.setFill(bodyColor);
        dolphin.getChildren().add(body);

        Shape stomach = pathFromSVG("stomach");
        stomach.setStroke(Color.TRANSPARENT);
        stomach.setFill(Color.rgb(232, 147, 150));
        dolphin.getChildren().add(stomach);

        Shape leftFin = pathFromSVG("leftFin");
        leftFin.setFill(Color.TRANSPARENT);
        dolphin.getChildren().add(leftFin);
        rotationAboutOrigin(leftFin, -30, 25, 45, 1874).play();

        dolphin.getChildren().add(pathFromSVG("nose"));

        Shape mouth = pathFromSVG("mouth");
        mouth.setFill(Color.rgb(220, 28, 38));
        dolphin.getChildren().add(mouth);

        Shape mouthThing = pathFromSVG("mouthThing");
        mouthThing.setStrokeWidth(1);
        dolphin.getChildren().add(mouthThing);

        Group leftEye = makeEye();
        leftEye.setTranslateX(35);
        leftEye.setTranslateY(-20);
        dolphin.getChildren().add(leftEye);

        Group rightEye = makeEye();
        rightEye.setTranslateX(55);
        rightEye.setTranslateY(-20);
        dolphin.getChildren().add(rightEye);

        Color circleColor = Color.rgb(115, 102, 171);
        Circle[] c = { new Circle(), new Circle(), new Circle() };
        c[0].setRadius(7);
        c[0].setCenterX(-40);
        c[0].setCenterY(-15);

        c[1].setRadius(6);
        c[1].setCenterX(-48);
        c[1].setCenterY(3);

        c[2].setRadius(5);
        c[2].setCenterX(-50);
        c[2].setCenterY(20);
        for (int i = 0; i < c.length; i++) {
            c[i].setFill(circleColor);
            c[i].setScaleX(0.9);
            c[i].setScaleY(0.9);
            dolphin.getChildren().add(c[i]);

            ScaleTransition circleScale = new ScaleTransition(Duration.seconds(1), c[i]);
            circleScale.setFromX(0.9);
            circleScale.setToX(1.1);
            circleScale.setFromY(0.9);
            circleScale.setToY(1.1);
            circleScale.setDelay(Duration.millis(1111 * i));
            circleScale.setCycleCount(Animation.INDEFINITE);
            circleScale.setAutoReverse(true);
            circleScale.play();
        }

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private Group makeEye() {
        Group group = new Group();

        Circle white = new Circle();
        white.setRadius(7);
        white.setFill(Color.WHITE);
        white.setStroke(Color.BLACK);
        white.setStrokeWidth(1);

        Circle black = new Circle();
        black.setRadius(4);
        black.setFill(Color.BLACK);
        black.setTranslateY(3);

        Circle glare = new Circle();
        glare.setRadius(1.5);
        glare.setFill(Color.WHITE);

        Path brow = new Path();
        brow.getElements().add(new MoveTo(3, -10));
        brow.getElements().add(new QuadCurveTo(0, -12, -3, -11));
        brow.setStroke(Color.BLACK);

        group.getChildren().add(white);
        group.getChildren().add(black);
        group.getChildren().add(glare);
        group.getChildren().add(brow);

        return group;
    }

    private Timeline rotationAboutOrigin(Shape shape, double x, double y, double angle, double millis) {
        Rotate rotation = new Rotate();
        rotation.setPivotX(x);
        rotation.setPivotY(y);
        shape.getTransforms().add(rotation);

        Timeline tm = new Timeline(
            new KeyFrame(Duration.ZERO, new KeyValue(rotation.angleProperty(), -angle/2)),
            new KeyFrame(Duration.millis(millis), new KeyValue(rotation.angleProperty(), angle/2))
        );
        tm.setCycleCount(Animation.INDEFINITE);
        tm.setAutoReverse(true);
        return tm;
    }

    private Shape pathFromSVG(String id) throws Exception {
        if (dolphinSVG == null) {
            DocumentBuilder builder = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder();

            dolphinSVG = builder.parse(dolphinPath);
        }

        NodeList elements = dolphinSVG.getElementsByTagName("path");
        for (int i = 0; i < elements.getLength(); i++) {
            Node node = elements.item(i);
            NamedNodeMap attributes = node.getAttributes();
            if (attributes.getNamedItem("id").getNodeValue().equals(id)) {
                String pathStr = attributes.getNamedItem("d").getNodeValue();

                SVGPath path = new SVGPath();
                path.setContent(pathStr);
                path.setStrokeWidth(2);
                path.setStroke(Color.BLACK);
                path.setStrokeLineCap(StrokeLineCap.ROUND);
                path.setFill(Color.TRANSPARENT);
                return path;
            }
        }

        throw new Exception("Id not found");
    }
}
